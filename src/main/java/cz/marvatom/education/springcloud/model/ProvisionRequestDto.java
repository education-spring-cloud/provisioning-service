package cz.marvatom.education.springcloud.model;

import java.time.LocalDateTime;

public class ProvisionRequestDto {
    private int id;
    private LocalDateTime created;
    private int numOfPsc;
    private int bookId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public int getNumOfPsc() {
        return numOfPsc;
    }

    public void setNumOfPsc(int numOfPsc) {
        this.numOfPsc = numOfPsc;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }
}
