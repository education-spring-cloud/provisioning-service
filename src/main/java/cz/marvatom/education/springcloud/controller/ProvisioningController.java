package cz.marvatom.education.springcloud.controller;

import cz.marvatom.education.springcloud.model.ProvisionRequestDto;
import cz.marvatom.education.springcloud.service.ProvisioningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProvisioningController implements ProvisioningApi{

    @Autowired
    ProvisioningService provisioningService;

    @Override
    public List<ProvisionRequestDto> getAllRequests() {
        return provisioningService.getAllRequests();
    }

    @Override
    public ProvisionRequestDto fill(int bookId) {
        int numOfPcs = 1 + (int)(Math.random() * (20)); //random between 1 and 20
        return provisioningService.fillBook(bookId, numOfPcs);
    }
}
