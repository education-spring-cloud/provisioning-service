package cz.marvatom.education.springcloud.service;

import cz.marvatom.education.springcloud.model.ProvisionRequestDto;

import java.util.List;

public interface ProvisioningService {
    ProvisionRequestDto fillBook(int bookId, int numOfPcs);
    List<ProvisionRequestDto> getAllRequests();
}
