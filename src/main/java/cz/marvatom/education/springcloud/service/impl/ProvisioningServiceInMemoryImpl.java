package cz.marvatom.education.springcloud.service.impl;

import cz.marvatom.education.springcloud.model.ProvisionRequestDto;
import cz.marvatom.education.springcloud.service.ProvisioningService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class ProvisioningServiceInMemoryImpl implements ProvisioningService {
    AtomicInteger idGen = new AtomicInteger(1);
    private List<ProvisionRequestDto> requests = Collections.synchronizedList(new ArrayList<>());

    @Override
    public ProvisionRequestDto fillBook(int bookId, int numOfPcs) {
        ProvisionRequestDto newRequest = new ProvisionRequestDto();
        newRequest.setId(idGen.getAndAdd(1));
        newRequest.setCreated(LocalDateTime.now());
        newRequest.setBookId(bookId);
        newRequest.setNumOfPsc(numOfPcs);
        synchronized (requests) {
            requests.add(newRequest);
        }
        return newRequest;
    }

    @Override
    public List<ProvisionRequestDto> getAllRequests() {
        return requests;
    }
}
