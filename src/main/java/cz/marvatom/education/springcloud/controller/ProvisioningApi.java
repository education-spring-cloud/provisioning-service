package cz.marvatom.education.springcloud.controller;

import cz.marvatom.education.springcloud.model.ProvisionRequestDto;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

public interface ProvisioningApi {
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    List<ProvisionRequestDto> getAllRequests();

    @PostMapping(value = "/{bookId}", produces = MediaType.APPLICATION_JSON_VALUE)
    ProvisionRequestDto fill(@PathVariable int bookId);
}
