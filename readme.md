# Provisioning service
This service handles requests to fill the warehouse. 

## Changelog
### Version 1.0
This version contains REST API.  

## Docker 
To build image from this directory:
```
docker build --tag gopas/provisioning-service .
```
And run it:
```
docker run -d --name provisioning-service -p9559:9559 gopas/provisioning-service
```
Stop it:
```
docker stop provisioning-service
```
Remove container:
```
docker rm provisioning-service
```